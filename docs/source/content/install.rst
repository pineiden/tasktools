.. toctree::
   :maxdepth: 2


Installing the Module
=====================

To install the module you have different options

--------------
Developer Mode
--------------

If you want to work with the last modifications, when the project is
updated, you can clone and install in dev mode (on the terminal).

.. code-block:: bash

                git clone https://gitlab.com/pineiden/tasktools
                python setup.py develop

----------------
Install with PIP
----------------

If you want to work with the stable release you can install with pip
in your virtualenv_ .

.. _virtualenv: https://docs.python.org/3/library/asyncio.html

.. code-block:: bash

                pip install tasktools

Or download the package from the pipy_ platform.

.. _pipy: https://pypi.org

.. code-block:: bash

                wget https://files.pythonhosted.org/packages/05/31/ee3981f7c4d6ee4968c53e40e0ed77089c50861cbe1960553006d0923a3f/tasktools-0.5.tar.gz
                python setup.py install





  
