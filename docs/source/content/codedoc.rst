Code's Documentation
====================

These documentation came from the source code.


The Async While to be a Wheel
-----------------------------


Those are the main functions to enable the async while. The core functions
to create asynchronous code. Are like the gears for a big future machine.

.. autofunction:: tasktools.taskloop.coromask

.. autofunction:: tasktools.taskloop.renew

.. autofunction:: tasktools.taskloop.renew_quamash

.. autofunction:: tasktools.taskloop.simple_fargs

.. autofunction:: tasktools.taskloop.simple_fargs_out


The Scheduler explained Step by Step
------------------------------------

The Scheduler is a like *abstract class* the enables a set of features that you have to
inherit to your own class.

.. autoclass:: tasktools.scheduler.TaskScheduler
   :members:

The Assignator role
-------------------

This class works fine, you don't have to implement a subclass of this, you have to use your
Scheduler class on it.

.. autoclass:: tasktools.assignator.TaskAssignator
   :members:
