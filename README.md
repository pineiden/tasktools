Asyncio Tasks Tools
======================

The [asyncio](https://docs.python.org/3/library/asyncio.html) module,
from standar library since 3.4, is basic for the coroutines and
tasks. You need to study first this module because it's not really easy.

This module allows you to work with asyncio coroutines and schedule some
tasks.

These tasks can be build in a really generic way, like an independet loop.

The tools in this module can help you to manage your coroutines and succeed.

The 'how to use' on [documentation](../doc/tasktools.pdf) file.


How to install
-----------------

To install, in your virtualenvironment, execute pip:

```
pip install git+[url]
```

Or, using poetry:

```
poetry install
```

Install from *pypi*

```
pip install tasktools
```


