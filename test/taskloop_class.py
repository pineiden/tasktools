import asyncio
from datetime import datetime, timedelta
from tasktools.taskloop import TaskLoop

"""
Este es un ejemplo de uso de la clase Taskloop,
que permite controlar una corutina en su reproducción

- START: habilita comienzo
- CONTINUA: si estaba pausada, continua
- PAUSE: se pausa la reproducción
- STOP: se detiene el loop

Para eso se presenta como ejemplo este caso, ambas corrutinas
son asociadas a Taskloop.
"""


async def timeline(*args, **kwargs):
    start_time = kwargs.get('start_time')
    end_time = kwargs.get('end_time')
    controled = kwargs.get('controled')
    now = datetime.now()
    if start_time <= now <= end_time:
        # segundos del minuto
        seconds = now.second
        if seconds < 30 and controled.control not in {'CONTINUE', 'START'}:
            print("Continued", controled)
            controled.task_continue()
            print("Controled continue", controled.control)
        elif seconds >= 30 and controled.control != 'PAUSE':
            controled.pause()
            print("Paused", controled, controled.control)
    else:
        print("Deteniendo tarea")
        controled.stop()
        kwargs['taskloop'].stop()
        loop = asyncio.get_running_loop()
        # las tareas que están activas dentro del loop
        # se deben cancelar para poder cerrar correctamente
        # el loop que las contiene
        if loop.is_running():
            tasks = asyncio.all_tasks()
            for task in tasks:
                task.cancel()
            loop.stop()
    await asyncio.sleep(1)
    return args, kwargs


async def showtime(*args, **kwargs):
    print("="*10)
    print(datetime.now())
    print("="*10)
    await asyncio.sleep(2)
    return args, kwargs


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    showtime_loop = TaskLoop(showtime, [],
                             {"name": "showtime"},
                             **{"name": "showtime"})
    start = datetime.now()
    end = start + timedelta(seconds=120)
    print("Controled:::", showtime_loop)
    timeline_loop = TaskLoop(timeline, [],
                             {'start_time': start,
                              'end_time': end,
                              "controled": showtime_loop,
                              "name": "timeline"},
                             **{"name": "timeline"})
    print("Starting...")
    task_timeline = timeline_loop.create()
    task_showtime = showtime_loop.create()
    try:
        loop.run_forever()
    finally:
        loop.run_until_complete(loop.shutdown_asyncgens())
        loop.close()
