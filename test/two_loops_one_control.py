import asyncio
from datetime import datetime, timedelta
from tasktools.taskloop import TaskLoop
from tasktools.assignator import timestamp, now
import random
import time


async def say_hello(name, queue, last_check, **kwargs):
    cnow = now()
    if cnow > last_check + timedelta(seconds=20):
        await queue.put(("hello-task", cnow))
    value = random.randint(0, 100)
    print(cnow, f"Hello {name}, randmon value {value}!")
    sleep = 2 + value
    print(now(), f"Sleeping {sleep}")
    await asyncio.sleep(sleep)
    print(now(), f"END Sleeping {sleep}")

    return (name, queue, last_check), kwargs

"""
This tests allows you to understand how to control the taskloop async
loop.
Can be monitored, paused and cancelled.
"""


async def control_loop(control, queue, last_check, tasks, cancelled, **kwargs):
    task_name = ""
    if control == 0:
        for task_name, task in tasks.items():
            task.create()
        control = 1
    cnow = now()
    if not queue.empty():
        for i in range(queue.qsize()):
            task_name, check = await queue.get()
            last_check[task_name] = check
        queue.task_done()
    for task_name, check in last_check.items():
        if task_name not in cancelled:
            if cnow > check + timedelta(seconds=40):
                task = tasks.get(task_name)
                cancelled[task_name] = task
                print(cnow, f"Cancelling task {task_name}")
                task.finish()
                result = None
                task.cancel()
                try:
                    await task
                    print(cnow, "Result stopping task", result)
                except (asyncio.CancelledError, asyncio.InvalidStateError) as e:
                    print(cnow, "Exception", e)

    print(cnow, f"Control loop {cnow}!")
    await asyncio.sleep(10)
    return (control, queue, last_check, tasks, cancelled), kwargs


if __name__ == "__main__":
    cnow = now()
    name = input("Dime tu nombre:")
    queue = asyncio.Queue()
    args = [name, queue, cnow]
    kwargs = {}
    task_hello = TaskLoop(say_hello, args, kwargs, name="hello-task")
    args = [0, queue, {"hello-task": cnow}, {"hello-task": task_hello}, {}]
    task_control = TaskLoop(control_loop, args, kwargs, name="control-task")
    task_control.create()

    loop = asyncio.get_event_loop()
    if not loop.is_running():
        loop.run_forever()
